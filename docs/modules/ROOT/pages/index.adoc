= Gradle Antora Plugin Documentation

The Antora plugin for Gradle prepares a Node.js runtime and then uses the runtime to install and run Antora.
The plugin invokes Antora using its CLI (`antora`) by way of the `npx` command.

== Overview

The Antora plugin builds on the https://plugins.gradle.org/plugin/com.github.node-gradle.node[Node.js plugin] to provision a Node.js runtime, which, in turn, it uses to install and run Antora.
That means you don't need Node.js preinstalled to use this plugin.
All you need is Java in order to run Gradle.
Head over to the xref:use.adoc[] page to see how easy it is to get started.

== Features

The Antora plugin provides the following capabilities and features:

* Run Antora without having to install or configure Node.js.
* Specify xref:use.adoc#run-antora[select Antora CLI options directly from the Gradle call].
* Assign Antora environment variables and CLI options using the xref:configure.adoc[antora extension block] in [.path]_build.gradle_.
* Manage the version of Antora installed by the plugin using the `antora` extension block, [.path]_package.json_ (with or without [.path]_package-lock.json_), or a special comment syntax in a provided Antora playbook file.
* xref:packages.adoc[Manage additional Antora and Asciidoctor Node.js extension packages] using the `packages` property in [.path]_build.gradle_, [.path]_package.json_ (with or without [.path]_package-lock.json_), or a special comment syntax in a provided Antora playbook file.
* Distribute and centrally manage a local, author-oriented Antora playbook template with the plugin's xref:playbook-provider.adoc[playbook provider feature].
