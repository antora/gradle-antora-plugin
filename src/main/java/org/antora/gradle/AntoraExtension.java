package org.antora.gradle;

import groovy.lang.Closure;
import org.gradle.api.Project;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class AntoraExtension {
    private static final String DEFAULT_ANTORA_PLAYBOOK_FILE = "antora-playbook.yml";

    private final ListProperty<String> options;

    private final Property<File> playbook;

    private final AntoraPlaybookProvider playbookProvider;

    private final Project project;

    @Inject
    public AntoraExtension(ObjectFactory objects, Project project) {
        this.project = project;
        this.getDependencies().convention(Collections.emptyMap());
        this.getEnvironment().convention(Collections.emptyMap());
        this.options = objects.listProperty(String.class).convention(Collections.emptyList());
        this.playbook = objects.property(File.class).convention(project.file(DEFAULT_ANTORA_PLAYBOOK_FILE));
        this.playbookProvider = objects.newInstance(AntoraPlaybookProvider.class);
    }

    @Deprecated
    public void setArguments(List<String> arguments) {
        this.getOptions().set(arguments);
    }

    @Deprecated
    public MapProperty<String, String> getDependencies() {
        return this.getPackages();
    }

    public abstract MapProperty<String, String> getEnvironment();

    public ListProperty<String> getOptions() {
        return this.options;
    }

    public void setOptions(List<String> options) {
        this.options.set(options);
    }

    public void setOptions(Map<String, Object> options) {
        List<String> optionsList = new ArrayList<>();
        options.forEach((key, value) -> {
            String name = "--" + key;
            if (value instanceof String) {
                optionsList.addAll(List.of(name, (String) value));
            } else if (value instanceof Boolean) {
                if (Boolean.TRUE.equals(value)) optionsList.add(name);
            } else if (value instanceof List && name.endsWith("s")) {
                String singularName = name.substring(0, name.length() - 1);
                ((List<?>) value).forEach(it -> {
                    if (it instanceof String) optionsList.addAll(List.of(singularName, (String) it));
                });
            } else if (value instanceof Map && "--attributes".equals(name)) {
                ((Map<?, ?>) value).forEach((n, v) -> {
                    if (!(n instanceof String)) return;
                    if (v instanceof Boolean) {
                        optionsList.addAll(List.of("--attribute", ((Boolean) v).booleanValue() ? (String) n : "!" + n));
                    } else if (v instanceof String) {
                        optionsList.addAll(List.of("--attribute", "".equals(v) ? (String) n : n + "=" + v));
                    }
                });
            }
        });
        this.setOptions(optionsList);
    }

    public abstract MapProperty<String, String> getPackages();

    public Property<File> getPlaybook() {
        return this.playbook;
    }

    public void setPlaybook(String path) {
        this.playbook.set(this.project.file(path));
    }

    @Deprecated
    public void setPlaybookFile(File file) {
        this.playbook.set(file);
    }

    public AntoraPlaybookProvider getPlaybookProvider() {
        return this.playbookProvider;
    }

    public void playbookProvider(Closure c) {
        project.configure(this.playbookProvider, c);
    }

    public abstract Property<String> getVersion();
}
